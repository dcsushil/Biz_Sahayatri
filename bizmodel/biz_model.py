import os

from utils import load_from_pickle_file
from lightfm import LightFM
import scipy.sparse as sp
import collections
import numpy as np
from lightfm.evaluation import precision_at_k, auc_score, recall_at_k


def map_kv(key, map_dict, inverse=False, default=None):
    if inverse:
        for k, v in map_dict.items():
            if key == v:
                return k
        return default
    else:
        return map_dict.get(key, default)


def train_model(model_dir):
    interaction_file = os.path.join(model_dir, "interactions.pkl")
    train = load_from_pickle_file(interaction_file)
    model = LightFM(no_components=32, learning_rate=0.001, loss='warp')
    if len(np.shape(train)) == 3:
        if type(train) != sp.lil.lil_matrix or type(train) != sp.coo.coo_matrix:
            train = train[:, :, 0]

    if type(train) != sp.lil.lil_matrix or type(train) != sp.coo.coo_matrix:
        train = sp.lil_matrix(train).tocoo()
        # test = sp.lil_matrix(test).tocoo()
    model.fit(train, epochs=30, num_threads=4)
    train_precision = precision_at_k(model, train, k=10).mean()
    train_recall = recall_at_k(model, train, k=10).mean()
    train_auc = auc_score(model, train).mean()
    print('Precision: train %.2f' % train_precision)
    print('Recall: train %.2f' % train_recall)
    print('AUC: train %.2f' % train_auc)

    return model


def prediction(model, model_dir, data, user_id):
    top_k = 20
    interaction_file = os.path.join(model_dir, "interactions.pkl")
    train = load_from_pickle_file(interaction_file)

    user_map = data["user_map"]
    item_map = data["item_map"]
    if len(np.shape(train)) == 3:
        if type(train) != sp.lil.lil_matrix or type(train) != sp.coo.coo_matrix:
            train = train[:, :, 0]
    if type(train) != sp.lil.lil_matrix or type(train) != sp.coo.coo_matrix:
        train = sp.lil_matrix(train).tocoo()
    user_ids = [user_id]
    item_ids = list(item_map.values())
    user_ids_ = [map_kv(k, user_map, inverse=False) for k in user_ids]
    assert None not in user_ids_, "Unknown user id(s)."
    n_users, n_items = train.shape
    predictions = model.predict(user_ids_, np.arange(n_items))
    predictions = np.reshape(predictions, (len(user_ids), len(item_ids)))
    sorted_index = np.argsort(-predictions, axis=1)
    model_results = []
    for i in range(len(user_ids)):
        model_results.append(list(map(list, zip(sorted_index[i], predictions[i][sorted_index[i]]))))
    results = model_results
    results = list(results)
    if top_k is not None:
        top_k = np.minimum(top_k, len(item_ids))
        for i in range(len(user_ids)):
            results[i] = results[i][:top_k]
    outputs = collections.OrderedDict()
    for i in range(len(user_ids)):
        items = results[i]
        if len(items) == 0 or len(items[0]) == 0:
            items = None
        outputs[user_ids[i]] = items
    return outputs


if __name__ == '__main__':
    model_dir = 'C:/Users/User/Downloads/retail_data_and_model'
    model_file = os.path.join(model_dir, "recome_model.pkl")
    model_data = load_from_pickle_file(model_file)
    if model_data is None:
        print("Error while loading model file.")
    else:
        model = train_model(model_dir)
        result = prediction(model, model_dir, model_data, 8207)
        if result is None:
            print("No prediction returned.")
        else:
            user_labels = model_data["user_labels"]
            item_labels = model_data["item_labels"]
            items = []
            for k, v in result.items():
                print("Given user: {} / {}".format(k, user_labels[k]))
                if v is not None:
                    for item, score in v:
                        if item in item_labels:
                            items.append({'id': item, "name": item_labels[item], "score": score})
            print(items)