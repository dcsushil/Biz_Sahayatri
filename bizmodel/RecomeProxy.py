import logging
import sys
from recome import Recommender
import numpy as np
import time
import  traceback


class RecomeProxy(object):
    __instance = None
    __is_init = False

    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = object.__new__(cls)
            cls.__instance.name = 'Recome Proxy Singleton'
        return cls.__instance

    def __init__(self, model_dir):
        self.model_dir = model_dir
        if not self.__is_init:
            self.__init_recome_engine()

    def __init_recome_engine(self):
        try:
            self.recome = Recommender(model_dir=self.model_dir)
            self.__is_init = True
            logging.debug("Recommendation engine initialized with model : %s", self.model_dir)
        except Exception as e:
            logging.error("Error while initializing engine with model : %s. Error : %s", self.model_dir, str(e))
            sys.exit()

    def recommendation(self, user_id):
        try:
            users = [user_id]
            contexts = []
            for _ in range(len(users)):
                # contexts.append([1530123300000])
                contexts.append([np.int64(time.time() * 1000)])
                # contexts.append([str_to_timestamp("2018-07-02", tz="Asia/Kathmandu")])
            return self.recome.predict(user_ids=users, item_ids=None, top_k=20, contexts=None, context_only=False)

        except Exception as e:
            traceback.print_exc()
            return None