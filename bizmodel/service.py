from RecomeProxy import RecomeProxy
import traceback

class RecomeHandler(object):
    def __init__(self, model_dir):
        self.recome_proxy = RecomeProxy(model_dir=model_dir)

    def recommendation(self, user_id):
        try:
            outputs = self.recome_proxy.recommendation(user_id)
            if outputs is None:
                print("No output found.")
                return False
            for k, v in outputs.items():
                print("Given user: %d/%s".format(k, self.recome_proxy.recome.user_labels[k]))
                if v is not None:
                    items = []
                    print(v)
                    for item, score in v:
                        result_item = {}
                        result_item.id = item
                        result_item.name = self.recome_proxy.recome.item_labels[item]
                        result_item.score = score
                        items.append(result_item)
                    print(items)
                else:
                    print("Error in value")
        except Exception as e:
            traceback.print_exc()

if __name__ == '__main__':
    # model = 'C:/Users/User/Downloads/retail_data_and_model.tar/retail_data_and_model'
    model = 'C:/Users/User/Downloads/retail_data_and_model'
    recommendation = RecomeHandler(model_dir=model)
    recommendation.recommendation(8208)

