
import logging
import os
import time

import numpy as np
import pandas as pd
import scipy.sparse as sp
from pandas import HDFStore
from sklearn.model_selection import train_test_split

from utils import save_to_pickle_file, load_from_pickle_file, map_kv, timestamp_to_datetime, min_max_normalize

logger = logging.getLogger(__name__)

l_time = "time"
l_timestamp = "timestamp"
l_user_id = "outlet_id"
l_item_id = "sku_id"
l_item_name = "sku_name"
l_quantity = "quantity"
l_sub_brand = "sub_brand"
l_brand = "brand"
l_product_group = "product_group"
l_principal = "principal"
l_user_name = "outlet_name"
l_user_category = "category_name"
l_price = "price"
l_order_status = "order_status"
l_rate = "rate"
l_gross_price = "gross_price"
l_promotion = "promotion"
l_bill_discount = "bill_discount"
l_no_order_reason = "reason"
l_no_order_count = "no_order_count"
l_order_difference_days = "order_difference_days"

day_map = {6: "sun", 0: "mon", 1: "tue", 2: "wed", 3: "thu", 4: "fri", 5: "sat"}  # zoda-time (0 = Monday)
month_map = {1: "jan", 2: "feb", 3: "mar", 4: "apr", 5: "may", 6: "jun", 7: "jul", 8: "aug", 9: 'sep',
             10: "oct",
             11: "nov", 12: "dec"}
weekends = ["fri", "sat"]
context_map = {"quantity": 0, "sun": 1, "mon": 2, "tue": 3, "wed": 4, "thu": 5, "fri": 6, "sat": 7,
               "weekend": 8,
               "jan": 9, "feb": 10, "mar": 11, "apr": 12, "may": 13, "jun": 14, "jul": 15, "aug": 16, "sep": 17,
               "oct": 18, "nov": 19, "dec": 20}
sales_transaction_context_map = {"price": 0, "sun": 1, "mon": 2, "tue": 3, "wed": 4, "thu": 5, "fri": 6, "sat": 7,
                                 "weekend": 8,
                                 "jan": 9, "feb": 10, "mar": 11, "apr": 12, "may": 13, "jun": 14, "jul": 15, "aug": 16,
                                 "sep": 17,
                                 "oct": 18, "nov": 19, "dec": 20, "quantity": 21}

sales_transaction_context_list = ["price", "sun", "mon", "tue", "wed", "thu", "fri", "sat", "weekend", "jan", "feb",
                                  "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec", "quantity"]

tz = "Asia/Kathmandu"


def save_to_hdf(files, output_file):
    store = HDFStore(output_file)
    logger.info("Saving files to hdfs store: {}".format(output_file))
    for file in files:
        basename = os.path.splitext(os.path.basename(file))[0]
        df = pd.read_csv(file)
        if l_time in df.columns:
            df[l_time] = df[l_time].apply(lambda x: pd.Timestamp(x).tz_localize(tz))
            df[l_timestamp] = np.int64(df[l_time].astype(np.int64) / 1e6)
        store[basename] = df
        logger.info("\t Successfully saved: " + basename)
    store.close()


def save_retail_dataset_to_hfd(data_dir):
    paths = [os.path.join(data_dir, "transactions.csv"), os.path.join(data_dir, "item_catalog.csv"),
             os.path.join(data_dir, "user_catalog.csv")]
    for path in paths:
        assert os.path.exists(path)

    save_to_hdf(paths, os.path.join(data_dir, "recome_dataset.h5"))


def _parse_interactions(data, uids_map, iids_map):
    for row in data:
        uid, iid, rate, quantity, gross_price, price, time, event_id, event, timestamp = row
        uid_ = map_kv(uid, uids_map, inverse=False)
        iid_ = map_kv(iid, iids_map, inverse=False)
        if uid_ is None or iid_ is None:
            continue
        yield uid_, iid_, quantity, event_id, timestamp


def _build_interaction_matrix_v0(rows, cols, data):
    tic = time.time()
    mat = sp.lil_matrix((rows, cols), dtype=np.int32)
    for uid, iid, quantity, event_id, timestamp in data:
        if event_id == 3:
            mat[uid, iid] += quantity
        elif event_id == 5:
            mat[uid, iid] -= quantity
    toc = (time.time() - tic)
    logger.info("Interaction feature creation time: {:.3f} sec.".format(toc))
    return mat.tocoo()


def _build_interaction_matrix(rows, cols, data, context=True):
    if context:
        return _build_context_interaction_matrix(rows, cols, data)

    tic = time.time()
    start = time.time()
    mat = np.zeros((rows, cols), dtype=np.int32)
    i = 0
    for uid, iid, quantity, event_id, timestamp in data:
        if event_id == 3:
            mat[uid, iid] += quantity
        elif event_id == 5:
            mat[uid, iid] -= quantity
        if i % 1000 == 0:
            toc = (time.time() - tic)
            tic = time.time()
            logger.info("Processed {} rows, time: {:.3f} sec.".format(i, toc))
        i += 1

    mat = sp.lil_matrix(mat)
    end = time.time() - start
    logger.info("Interaction feature creation time: {:.3f} sec.".format(end))
    return mat.tocoo()


def _build_context_interaction_matrix(rows, cols, data):
    tic = time.time()
    start = time.time()
    mat = np.zeros(shape=(rows, cols, len(context_map)), dtype=np.int32)
    i = 0
    for uid, iid, quantity, event_id, timestamp in data:
        dt = timestamp_to_datetime(timestamp=timestamp, tz=tz)
        day = day_map[dt.weekday()]
        month = month_map[dt.month]
        weekend = True if day in weekends else False

        mat[uid, iid, context_map["weekend"]] += 1 if weekend else 0
        mat[uid, iid, context_map[day]] += 1
        mat[uid, iid, context_map[month]] += 1
        if event_id == 3:
            mat[uid, iid, context_map["quantity"]] += quantity
        elif event_id == 5:
            mat[uid, iid, context_map["quantity"]] -= quantity

        if i % 1000 == 0:
            toc = (time.time() - tic)
            tic = time.time()
            logger.info("Processed {} rows, time: {:.3f} sec.".format(i, toc))
        i += 1
    end = time.time() - start
    logger.info("Interaction feature creation time: {:.3f} sec.".format(end))

    return mat


def create_context_feature(context):
    """
    :param context: [timestamp, location, ...]
    :return:
    """
    dt = timestamp_to_datetime(timestamp=context[0], tz=tz)
    day = day_map[dt.weekday()]
    month = month_map[dt.month]
    weekend = True if day in weekends else False
    mat = np.zeros(len(context_map), np.int32)
    mat[context_map["weekend"]] = 1 if weekend else 0
    mat[context_map[day]] = 1
    mat[context_map[month]] = 1
    return mat


def create_context_features(contexts):
    features = []
    if contexts is not None:
        for context in contexts:
            features.append(create_context_feature(context))
    return np.array(features)


def _parse_item_metadata(num_items,
                         item_metadata_raw, iids_map):
    assert num_items == len(iids_map)
    sub_brands = item_metadata_raw[l_sub_brand].unique()
    sub_brands = ["sub_brand: " + sb for sb in sub_brands]
    brands = item_metadata_raw[l_brand].unique()
    brands = ["brand: " + b for b in brands]
    pgs = item_metadata_raw[l_product_group].unique()
    pgs = ["product_group: " + pg for pg in pgs]
    principals = item_metadata_raw[l_principal].unique()
    principals = ["principal: " + p for p in principals]
    item_meta_features = sub_brands + brands + pgs + principals

    id_feature_labels = np.empty(num_items, dtype=np.object)
    meta_feature_labels = np.array(item_meta_features)

    id_features = sp.identity(num_items,
                              format='csr',
                              dtype=np.float32)
    meta_features = sp.lil_matrix((num_items, len(item_meta_features)),
                                  dtype=np.float32)
    for _, row in item_metadata_raw.iterrows():
        iid_ = map_kv(row[l_item_id], iids_map, inverse=False, default=None)

        if iid_ is not None:
            id_feature_labels[iid_] = row[l_item_name]

            elements = ["sub_brand: " + row[l_sub_brand], "brand: " + row[l_brand],
                        "product_group: " + row[l_product_group],
                        "principal: " + row[l_principal]]
            indexes = list(map(lambda x: item_meta_features.index(x), elements))
            for fid in indexes:
                meta_features[iid_, fid] = 1.0

    return (id_features, id_feature_labels,
            meta_features.tocsr(), meta_feature_labels)


def _parse_user_metadata(num_users,
                         user_metadata_raw, uids_map):
    assert num_users == len(uids_map)
    categories = user_metadata_raw[l_user_category].unique()
    categories = ["categories: " + value for value in categories]

    user_meta_features = categories

    id_feature_labels = np.empty(num_users, dtype=np.object)
    meta_feature_labels = np.array(user_meta_features)

    id_features = sp.identity(num_users,
                              format='csr',
                              dtype=np.float32)
    meta_features = sp.lil_matrix((num_users, len(user_meta_features)),
                                  dtype=np.float32)
    for _, row in user_metadata_raw.iterrows():
        uid_ = map_kv(row[l_user_id], uids_map, inverse=False, default=None)

        if uid_ is not None:
            id_feature_labels[uid_] = row[l_user_name]

            elements = ["categories: " + row[l_user_category]]
            indexes = list(map(lambda x: user_meta_features.index(x), elements))
            for fid in indexes:
                meta_features[uid_, fid] = 1.0

    return (id_features, id_feature_labels,
            meta_features.tocsr(), meta_feature_labels)


def combine_features(id_and_meta_features, indicator_features=True, meta_features=False):
    if indicator_features and not meta_features:
        features = id_and_meta_features[0]
        labels = id_and_meta_features[1]
    elif meta_features and not indicator_features:
        features = id_and_meta_features[2]
        labels = id_and_meta_features[3]
    else:
        features = sp.hstack([id_and_meta_features[0], id_and_meta_features[2]]).tocsr()
        labels = np.concatenate((id_and_meta_features[1],
                                 id_and_meta_features[3]))
    return features, labels


def create_dataset(data_dir, indicator_features=True, meta_features=False, context=True):
    logger.info("Creating the dataset for recommendation ... ")

    meta_file = os.path.join(data_dir, "meta.pkl")
    interactions_file = os.path.join(data_dir, "interactions.pkl")
    item_features_file = os.path.join(data_dir, "item_features.pkl")
    user_features_file = os.path.join(data_dir, "user_features.pkl")

    use_item_and_user_info_from_interaction = False
    force = False
    normalize = True
    if force or not os.path.exists(interactions_file) or not os.path.exists(item_features_file) or not os.path.exists(
            user_features_file):
        data_store_file = os.path.join(data_dir, "recome_dataset.h5")
        if not os.path.exists(data_store_file):
            save_retail_dataset_to_hfd(data_dir=data_dir)

        store = HDFStore(data_store_file)
        tdf = store["transactions"]
        udf = store["user_catalog"]
        idf = store["item_catalog"]

        if use_item_and_user_info_from_interaction:
            uids = np.sort(tdf[l_user_id].unique())
            iids = np.sort(tdf[l_item_id].unique())
        else:
            uids = np.sort(udf[l_user_id].unique())
            iids = np.sort(idf[l_item_id].unique())
        uids_map = {}
        iids_map = {}
        for i in range(len(uids)):
            uids_map[uids[i]] = i
        for i in range(len(iids)):
            iids_map[iids[i]] = i

        num_users = len(uids)
        num_items = len(iids)
        logger.info("From transactions:: Num users: {}, Num items: {}".format(num_users, num_items))
        logger.info("From Metadata:: Num users: {}, Num items: {}".format(udf.shape[0], idf.shape[0]))
        iids_labels = {}
        for i, row in idf.iterrows():
            iids_labels[row[l_item_id]] = row[l_item_name]

        uids_labels = {}
        for i, row in udf.iterrows():
            uids_labels[row[l_user_id]] = row[l_user_name]

        save_to_pickle_file(meta_file, (uids_map, iids_map, uids_labels, iids_labels))
        logger.info("General meta information is saved to: {}".format(meta_file))
    else:
        store = None
        tdf = None
        idf = None
        udf = None
        num_users = 0
        num_items = 0
        logger.info("Loading meta information from: {}".format(meta_file))
        (uids_map, iids_map, uids_labels, iids_labels) = load_from_pickle_file(meta_file)

    logger.info("Loading interaction features from: {}".format(interactions_file))
    interactions = load_from_pickle_file(interactions_file)
    if interactions is None:
        logger.info("No interaction features are available!")
        logger.info("Creating interaction features...")
        interactions = _build_interaction_matrix(num_users, num_items,
                                                 _parse_interactions(tdf.head(500000).values, uids_map=uids_map,
                                                                     iids_map=iids_map), context=context)
        save_to_pickle_file(interactions_file, interactions)
        logger.info("Interaction features are saved to: {}".format(interactions_file))

    # normalize interactions
    if normalize:
        for i in range(len(interactions)):
            interactions[i, :, 0] = min_max_normalize(interactions[i, :, 0], map_max=20, map_min=0, categorical=True)

    logger.info("Loading item features: {}".format(item_features_file))
    item_meta = load_from_pickle_file(item_features_file)
    if item_meta is None:
        logger.info("No item features are available!")
        logger.info("Creating item features")
        (item_id_features, item_id_feature_labels, item_meta_features_matrix,
         item_meta_feature_labels) = _parse_item_metadata(num_items, idf, iids_map)
        assert item_id_features.shape == (num_items, len(item_id_feature_labels))
        assert item_meta_features_matrix.shape == (num_items, len(item_meta_feature_labels))
        save_to_pickle_file(item_features_file, (item_id_features, item_id_feature_labels, item_meta_features_matrix,
                                                 item_meta_feature_labels))
        logger.info("Item features are saved to: {}".format(item_features_file))
    else:
        (item_id_features, item_id_feature_labels, item_meta_features_matrix, item_meta_feature_labels) = item_meta

    item_features, item_feature_labels = combine_features(
        (item_id_features, item_id_feature_labels, item_meta_features_matrix, item_meta_feature_labels),
        indicator_features=indicator_features, meta_features=meta_features)

    logger.info("Loading user features: {}".format(user_features_file))
    user_meta = load_from_pickle_file(user_features_file)
    if user_meta is None:
        logger.info("No user features are available!")
        logger.info("Creating user features")
        (user_id_features, user_id_feature_labels, user_meta_features_matrix,
         user_meta_feature_labels) = _parse_user_metadata(num_users, udf, uids_map)
        assert user_id_features.shape == (num_users, len(user_id_feature_labels))
        assert user_meta_features_matrix.shape == (num_users, len(user_meta_feature_labels))
        save_to_pickle_file(user_features_file, (user_id_features, user_id_feature_labels, user_meta_features_matrix,
                                                 user_meta_feature_labels))
        logger.info("User features are saved to: {}".format(user_features_file))
    else:
        (user_id_features, user_id_feature_labels, user_meta_features_matrix, user_meta_feature_labels) = user_meta

    user_features, user_feature_labels = combine_features(
        (user_id_features, user_id_feature_labels, user_meta_features_matrix, user_meta_feature_labels),
        indicator_features=indicator_features, meta_features=meta_features)

    if store is not None:
        store.close()

    data = {'train': interactions,
            'test': interactions,
            'item_features': item_features,
            'item_feature_labels': item_feature_labels,
            'item_labels': item_id_feature_labels,
            "item_map": iids_map,
            "item_labels": iids_labels,
            'user_features': user_features,
            'user_feature_labels': user_feature_labels,
            'user_labels': user_id_feature_labels,
            "user_map": uids_map,
            "user_labels": uids_labels,
            "is_indicator_features": indicator_features,
            "is_meta_features": meta_features
            }

    return data


# Deprecated- use create_dataset
def create_context_dataset(data_dir, indicator_features=True, meta_features=False):
    logger.info("Creating the dataset for recommendation ... ")

    meta_file = os.path.join(data_dir, "meta.pkl")
    interactions_file = os.path.join(data_dir, "context_interactions.pkl")
    item_features_file = os.path.join(data_dir, "item_features.pkl")
    user_features_file = os.path.join(data_dir, "user_features.pkl")

    use_item_and_user_info_from_interaction = False
    force = False
    if force or not os.path.exists(interactions_file) or not os.path.exists(item_features_file) or not os.path.exists(
            user_features_file):
        data_store_file = os.path.join(data_dir, "recome_dataset.h5")
        if not os.path.exists(data_store_file):
            save_retail_dataset_to_hfd(data_dir=data_dir)

        store = HDFStore(data_store_file)
        tdf = store["transactions"]
        udf = store["user_catalog"]
        idf = store["item_catalog"]

        if use_item_and_user_info_from_interaction:
            uids = np.sort(tdf[l_user_id].unique())
            iids = np.sort(tdf[l_item_id].unique())
        else:
            uids = np.sort(udf[l_user_id].unique())
            iids = np.sort(idf[l_item_id].unique())
        uids_map = {}
        iids_map = {}
        for i in range(len(uids)):
            uids_map[i] = uids[i]
        for i in range(len(iids)):
            iids_map[i] = iids[i]

        num_users = len(uids)
        num_items = len(iids)
        logger.info("From transactions:: Num users: ", num_users, "Num items:", num_items)
        logger.info("From Metadata:: Num users: ", udf.shape[0], "Num items:", idf.shape[0])
        iids_labels = {}
        for i, row in idf.iterrows():
            iids_labels[row[l_item_id]] = row[l_item_name]

        uids_labels = {}
        for i, row in udf.iterrows():
            uids_labels[row[l_user_id]] = row[l_user_name]

        save_to_pickle_file(meta_file, (uids_map, iids_map, uids_labels, iids_labels))
        logger.info("General meta information is saved to: {}".format(meta_file))
    else:
        store = None
        tdf = None
        idf = None
        udf = None
        num_users = 0
        num_items = 0
        logger.info("Loading meta information from: {}".format(meta_file))
        (uids_map, iids_map, uids_labels, iids_labels) = load_from_pickle_file(meta_file)

    logger.info("Loading interaction features from: {}".format(interactions_file))
    interactions = load_from_pickle_file(interactions_file)
    if interactions is None:
        logger.info("No interaction features are available!")
        logger.info("Creating interaction features...")
        interactions = _build_context_interaction_matrix(num_users, num_items,
                                                         _parse_interactions(tdf.head(1000).values, uids_map=uids_map,
                                                                             iids_map=iids_map))
        save_to_pickle_file(interactions_file, interactions)
        logger.info("Interaction features are saved to: {}".format(interactions_file))

    logger.info("Loading item features: {}".format(item_features_file))
    item_meta = load_from_pickle_file(item_features_file)
    if item_meta is None:
        logger.info("No item features are available!")
        logger.info("Creating item features")
        (item_id_features, item_id_feature_labels, item_meta_features_matrix,
         item_meta_feature_labels, iids_map_label) = _parse_item_metadata(num_items,
                                                                          idf,
                                                                          iids_map)
        assert item_id_features.shape == (num_items, len(item_id_feature_labels))
        assert item_meta_features_matrix.shape == (num_items, len(item_meta_feature_labels))
        save_to_pickle_file(item_features_file, (item_id_features, item_id_feature_labels, item_meta_features_matrix,
                                                 item_meta_feature_labels, iids_map_label))
        logger.info("Item features are saved to: {}".format(item_features_file))
    else:
        (item_id_features, item_id_feature_labels, item_meta_features_matrix, item_meta_feature_labels,
         iids_map_label) = item_meta

    item_features, item_feature_labels = combine_features(
        (item_id_features, item_id_feature_labels, item_meta_features_matrix, item_meta_feature_labels,
         iids_map_label), indicator_features=indicator_features, meta_features=meta_features)

    logger.info("Loading user features: {}".format(user_features_file))
    user_meta = load_from_pickle_file(user_features_file)
    if user_meta is None:
        logger.info("No user features are available!")
        logger.info("Creating user features")
        (user_id_features, user_id_feature_labels, user_meta_features_matrix,
         user_meta_feature_labels, uids_map_label) = _parse_user_metadata(num_users,
                                                                          udf,
                                                                          uids_map)
        assert user_id_features.shape == (num_users, len(user_id_feature_labels))
        assert user_meta_features_matrix.shape == (num_users, len(user_meta_feature_labels))
        save_to_pickle_file(user_features_file, (user_id_features, user_id_feature_labels, user_meta_features_matrix,
                                                 user_meta_feature_labels, uids_map_label))
        logger.info("User features are saved to: {}".format(user_features_file))
    else:
        (user_id_features, user_id_feature_labels, user_meta_features_matrix, user_meta_feature_labels,
         uids_map_label) = user_meta

    user_features, user_feature_labels = combine_features(
        (user_id_features, user_id_feature_labels, user_meta_features_matrix, user_meta_feature_labels,
         uids_map_label), indicator_features=indicator_features, meta_features=meta_features)

    if store is not None:
        store.close()

    data = {'train': interactions,
            'test': interactions,
            'item_features': item_features,
            'item_feature_labels': item_feature_labels,
            'item_labels': item_id_feature_labels,
            "item_map": iids_map,
            "item_map_label": iids_map_label,
            "item_labels": iids_labels,
            'user_features': user_features,
            'user_feature_labels': user_feature_labels,
            'user_labels': user_id_feature_labels,
            "user_map": uids_map,
            "user_map_label": uids_map_label,
            "user_labels": uids_labels,
            "is_indicator_features": indicator_features,
            "is_meta_features": meta_features
            }

    return data


# =================================================
# Sales order prediction dataset

def create_st_prediction_row_v1(user_id, item_id, context, feature_map):
    """
    Row without target <-- input for inference
    :param feature_map:
    :param user_id:
    :param item_id:
    :param context:
    :return:
    """
    row = sp.lil_matrix((1, len(feature_map) - 1), dtype=np.int32)
    row[0, feature_map["user_" + str(user_id)]] = 1
    row[0, feature_map["item_" + str(item_id)]] = 1

    if context is not None:
        dt = timestamp_to_datetime(timestamp=context[0], tz=tz)
        day = day_map[dt.weekday()]
        month = month_map[dt.month]
        weekend = True if day in weekends else False

        row[0, feature_map["context_" + str(day)]] = 1
        row[0, feature_map["context_" + str(month)]] = 1
        row[0, feature_map["context_weekend"]] = 1 if weekend else 0

    return row.tocoo()


def create_st_prediction_row(user_id, item_id, context, feature_map):
    """
    Row without target <-- input for inference
    :param feature_map:
    :param user_id:
    :param item_id:
    :param context:
    :return:
    """
    row = sp.lil_matrix((1, len(feature_map) - 1), dtype=np.int32)
    row[0, feature_map["user_" + str(user_id)]] = 1
    row[0, feature_map["item_" + str(item_id)]] = 1

    if context is not None:
        dt = timestamp_to_datetime(timestamp=context[0], tz=tz)
        day = dt.day
        month = dt.month
        weekday=dt.weekday()
        weekend= 1 if weekday in [4, 5] else 0

        row[0, feature_map["context_weekend"]] = weekend
        row[0, feature_map["context_weekday_" + str(weekday)]] = 1
        row[0, feature_map["context_month_" + str(month)]] = 1
        row[0, feature_map["context_day"]] = day
    row[0, feature_map["context_rate"]] = 0
    # row[0, feature_map["context_gross_price"]] = 0
    # row[0, feature_map["context_price"]] = 0
    row[0, feature_map["context_promotion"]] = 0
    # row[0, feature_map["context_bill_discount"]] = 0
    row[0, feature_map["context_no_order_count"]] = 0
    row[0, feature_map["context_order_difference_days"]] = 1

    print(row)
    return row.tocoo()

# def _create_st_data_row(data_row, feature_map):
#     """
#     :param data_row: [uid, iid, quantity, event_id, timestamp]
#     :param feature_map:
#     :return:
#     """
#     uid, iid, quantity, event_id, timestamp = data_row
#     if event_id != 3:
#         return None
#     row = sp.lil_matrix((1, len(feature_map)), dtype=np.int32)
#
#     dt = timestamp_to_datetime(timestamp=timestamp, tz=tz)
#     day = day_map[dt.weekday()]
#     month = month_map[dt.month]
#     weekend = True if day in weekends else False
#     row[0, feature_map["user_" + str(uid)]] = 1
#     row[0, feature_map["item_" + str(iid)]] = 1
#     row[0, feature_map["context_weekend"]] = 1 if weekend else 0
#     row[0, feature_map["context_" + str(day)]] = 1
#     row[0, feature_map["context_" + str(month)]] = 1
#     row[0, feature_map["context_quantity"]] = quantity
#     return row.tocoo()


# def _build_sales_transaction_matrix_v0(rows, cols, data, context=True):
#     """
#     Sales order prediction matrix
#     :param rows: number of users
#     :param cols: number of items
#     :param data:
#     :return:
#     """
#     tic = time.time()
#     num_features = rows + cols + len(sales_transaction_context_map)
#     mat = sp.lil_matrix((0, num_features), dtype=np.int32)
#     i = 0
#     for uid, iid, quantity, event_id, timestamp in data:
#         mat.resize(i + 1, num_features)
#         if event_id != 3:
#             continue
#         dt = timestamp_to_datetime(timestamp=timestamp, tz=tz)
#         day = day_map[dt.weekday()]
#         month = month_map[dt.month]
#         weekend = True if day in weekends else False
#         # print("context", uid, iid, event_id, weekend, day, month, quantity) print(uid, rows + iid, rows + cols +
#         # sales_transaction_context_map["weekend"] , rows + cols + sales_transaction_context_map[day], rows + cols +
#         # sales_transaction_context_map[month], rows + cols + sales_transaction_context_map["quantity"])
#         mat[i, uid] = 1
#         mat[i, rows + iid] = 1
#         mat[i, rows + cols + sales_transaction_context_map["weekend"]] = 1 if weekend else 0
#         mat[i, rows + cols + sales_transaction_context_map[day]] = 1
#         mat[i, rows + cols + sales_transaction_context_map[month]] = 1
#         mat[i, rows + cols + sales_transaction_context_map["quantity"]] = quantity
#
#         if i % 500 == 0:
#             print("Processed {} rows".format(i))
#         i += 1
#     toc = (time.time() - tic)
#     logger.info("Sales transaction feature creation time: {:.3f} sec.".format(toc))
#     return mat.tocoo()
#

# def _build_sales_transaction_matrix_v1(rows, cols, data, feature_map, context=True):
#     """
#     Sales order prediction matrix
#     :param rows: number of users
#     :param cols: number of items
#     :param data:
#     :return:
#     """
#     tic = time.time()
#     num_features = rows + cols + len(sales_transaction_context_map)
#     mat = sp.lil_matrix((0, num_features), dtype=np.int32)
#     i = 0
#     for uid, iid, quantity, event_id, timestamp in data:
#         mat.resize(i + 1, num_features)
#         if event_id != 3:
#             continue
#         dt = timestamp_to_datetime(timestamp=timestamp, tz=tz)
#         day = day_map[dt.weekday()]
#         month = month_map[dt.month]
#         weekend = True if day in weekends else False
#         mat[i, feature_map["user_" + str(uid)]] = 1
#         mat[i, feature_map["item_" + str(iid)]] = 1
#         mat[i, feature_map["context_weekend"]] = 1 if weekend else 0
#         mat[i, feature_map["context_" + str(day)]] = 1
#         mat[i, feature_map["context_" + str(month)]] = 1
#         mat[i, feature_map["context_quantity"]] = quantity
#
#         if i % 500 == 0:
#             print("Processed {} rows".format(i))
#         i += 1
#     toc = (time.time() - tic)
#     logger.info("Sales transaction feature creation time: {:.3f} sec.".format(toc))
#     return mat.tocoo()


def _build_sales_transaction_matrix(data, uids_map, iids_map, feature_map):
    """
    Sales order prediction matrix
    :param data:
    :return:
    """

    def _create_st_data_row(data_row):
        """
        :param data_row: [uid, iid, quantity, order_status, trans_date, rate, gross_price, price, promotion,
        bill_discount,reason, next_trans_date,no_order_count,order_difference_days,no_order_reason, day, month,
        weekday, weekend]
        :return:
        """

        if data_row is None:
            return None
        uid, iid, quantity, order_status, so_trans_date, trans_date, rate, gross_price, price, promotion, bill_discount, reason, next_trans_date, no_order_count, order_difference_days, no_order_reason, day, month, weekday, weekend = data_row
        uid = map_kv(uid, uids_map, inverse=False, default=None)
        iid = map_kv(iid, iids_map, inverse=False, default=None)
        if uid is None or iid is None:
            return None
        if order_status != 1:
            return None
        row = sp.lil_matrix((1, len(feature_map)), dtype=np.float32)
        row[0, feature_map["user_" + str(uid)]] = 1
        row[0, feature_map["item_" + str(iid)]] = 1
        row[0, feature_map["context_weekend"]] = weekend
        row[0, feature_map["context_weekday_" + str(weekday)]] = 1
        row[0, feature_map["context_month_" + str(month)]] = 1
        row[0, feature_map["context_day"]] = day
        row[0, feature_map["context_rate"]] = rate
        # row[0, feature_map["context_gross_price"]] = gross_price
        # row[0, feature_map["context_price"]] = price
        row[0, feature_map["context_promotion"]] = promotion
        # row[0, feature_map["context_bill_discount"]] = bill_discount
        row[0, feature_map["context_no_order_count"]] = no_order_count
        row[0, feature_map["context_order_difference_days"]] = order_difference_days
        row[0, feature_map["context_quantity"]] = quantity
        return row.tocoo()

    data = pd.DataFrame(data)
    data[l_no_order_count].fillna(0, inplace=True)
    data[l_order_difference_days].fillna(0, inplace=True)

    print(data.columns)
    data["trans_date"] = data['trans_date'].apply(lambda x: pd.to_datetime(x, format='%Y-%M-%d'))
    data["day"] = data["trans_date"].apply(lambda x: x.day)
    data["month"] = data["trans_date"].apply(lambda x: x.month)
    data["weekday"] = data["trans_date"].apply(lambda x: x.weekday())
    data["weekend"] = data["trans_date"].apply(lambda x: 1 if x.weekday() in [4, 5] else 0)

    print(data)
    i = 0
    miss = 0
    hit = 0
    features = []
    tic = time.time()
    start = time.time()
    for row in data.values:
        result = _create_st_data_row(row)
        if result is not None:
            features.append(result)
            hit += 1
        else:
            miss += 1

        if i % 500 == 0:
            toc = (time.time() - tic)
            tic = time.time()
            logger.info("Processed {} rows, hit: {} miss: {}, time: {:.3f} sec.".format(i, hit, miss, toc))
        i += 1

    data = []
    row = []
    col = []
    for i in range(len(features)):
        data.extend(features[i].data)
        col.extend(features[i].col)
        row.extend(np.repeat(i, len(features[i].col)))
    mat = sp.csr_matrix((data, (row, col)), dtype=np.float32)
    end = time.time() - start
    logger.info("Sales transaction feature creation time: {:.3f} sec.".format(end))
    return mat.tocoo()


def create_st_dataset(data_dir, test_split=0.2):
    logger.info("Creating the dataset for sales order prediction ... ")

    meta_file = os.path.join(data_dir, "sales_meta.pkl")
    sales_transaction_file = os.path.join(data_dir, "sales_order_dataset.csv")
    sales_transaction_feature_file = os.path.join(data_dir, "sales_transaction.pkl")
    user_meta_file = os.path.join(data_dir, "user_catalog.csv")
    item_meta_file = os.path.join(data_dir, "item_catalog.csv")
    check_valid_target = True
    use_item_and_user_info_from_interaction = False
    use_covered_users_only = True
    force = False
    if force or not os.path.exists(sales_transaction_feature_file):
        tdf = pd.read_csv(sales_transaction_file, na_values=['nan', 'null'])
        udf = pd.read_csv(user_meta_file, na_values=['nan', 'null'])
        idf = pd.read_csv(item_meta_file, na_values=['nan', 'null'])

        if use_item_and_user_info_from_interaction:
            uids = np.sort(tdf[l_user_id].unique())
            iids = np.sort(tdf[l_item_id].unique())
        else:
            uids = np.sort(udf[l_user_id].unique())
            iids = np.sort(idf[l_item_id].unique())

        if use_covered_users_only:
            uids = np.sort(np.array(covered_outlet_ids).astype(np.int32))

        uids_map = {}
        iids_map = {}
        for i in range(len(uids)):
            uids_map[uids[i]] = i
        for i in range(len(iids)):
            iids_map[iids[i]] = i

        num_users = len(uids)
        num_items = len(iids)
        logger.info("From Metadata:: Num users: {}, Num items: {}".format(udf.shape[0], idf.shape[0]))
        logger.info("Covered: Num users: {}, Num items: {}".format(num_users, num_items))

        iids_labels = {}
        for i, row in idf.iterrows():
            iids_labels[row[l_item_id]] = row[l_item_name]

        uids_labels = {}
        for i, row in udf.iterrows():
            uids_labels[row[l_user_id]] = row[l_user_name]

        feature_list = []

        for uid in uids_map.values():
            feature_list.append("user_" + str(uid))
        for iid in iids_map.values():
            feature_list.append("item_" + str(iid))

        # sales_transaction_contexts = ["weekday_0", "weekday_1", "weekday_2", "weekday_3", "weekday_4", "weekday_5",
        #                               "weekday_6", "weekend", "month_1", "month_2",
        #                               "month_3", "month_4", "month_5", "month_6", "month_7", "month_8", "month_9",
        #                               "month_10", "month_11", "month_12",
        #                               "day", "rate", "gross_price", "price", "promotion", "bill_discount",
        #                               "no_order_count", "order_difference_days", "quantity"]
        sales_transaction_contexts = ["weekday_0", "weekday_1", "weekday_2", "weekday_3", "weekday_4", "weekday_5",
                                      "weekday_6", "weekend", "month_1", "month_2",
                                      "month_3", "month_4", "month_5", "month_6", "month_7", "month_8", "month_9",
                                      "month_10", "month_11", "month_12",
                                      "day", "rate", "promotion",
                                      "no_order_count", "order_difference_days", "quantity"]

        for e in sales_transaction_contexts:
            feature_list.append("context_" + str(e))

        feature_map = {}
        for i, e in enumerate(feature_list):
            feature_map[e] = i

        save_to_pickle_file(meta_file, (uids_map, iids_map, uids_labels, iids_labels, feature_map))
        logger.info("General meta information is saved to: {}".format(meta_file))
    else:
        tdf = None
        logger.info("Loading meta information from: {}".format(meta_file))
        (uids_map, iids_map, uids_labels, iids_labels, feature_map) = load_from_pickle_file(meta_file)

    logger.info("Loading sales transactions features from: {}".format(sales_transaction_feature_file))
    interactions = load_from_pickle_file(sales_transaction_feature_file)
    if interactions is None:
        logger.info("No sales transactions features are available!")
        logger.info("Creating sales transaction features...")

        interactions = _build_sales_transaction_matrix(tdf, uids_map=uids_map, iids_map=iids_map,
                                                       feature_map=feature_map)
        save_to_pickle_file(sales_transaction_feature_file, interactions)
        logger.info("Sales transaction features are saved to: {}".format(sales_transaction_feature_file))

    # Train/Test split
    X = interactions.tocsc()[:, :-1]
    Y = np.array(interactions.tocsc()[:, -1].todense()).flatten()

    if check_valid_target:
        non_zeros = np.array(list(
            map(lambda x: (True if x[0] > 0 else False) if type(x) == list else (True if x > 0 else False), Y)))
        X = X[non_zeros]
        Y = Y[non_zeros]
    train_x, test_x, train_y, test_y = train_test_split(X, Y, test_size=test_split, random_state=42)

    data = {'train': (train_x, train_y),
            'test': (test_x, test_y),
            "item_map": iids_map,
            "item_labels": iids_labels,
            "user_map": uids_map,
            "user_labels": uids_labels,
            "feature_map": feature_map
            }

    return data


# Deprecated, use load-sales-order-data
def create_st_dataset_from_csv(filename, user_id=None, test_split=0.2):
    import pandas as pd
    # outlet_id, sku_id, rate, quantity, gross_price, price, time, event_id, event
    df = pd.read_csv(filename, usecols=None, engine='python', skiprows=0, skipfooter=0,
                     na_values=['nan', 'null'])
    # user_id = 55571
    if user_id is not None:
        df = df[df["outlet_id"] == user_id]
    df["date_time"] = pd.to_datetime(df['time'], format='%Y-%M-%d')
    df["day"] = df["date_time"].apply(lambda x: x.day)
    df["month"] = df["date_time"].apply(lambda x: x.month)
    df["weekday"] = df["date_time"].apply(lambda x: x.weekday())
    df["weekend"] = df["date_time"].apply(lambda x: 1 if x.weekday() in [4, 5] else 0)
    # columns: ['outlet_id', 'sku_id', 'rate', 'quantity', 'gross_price', 'price', 'time', 'event_id', 'event',
    # 'date_time',  'day', 'month', 'weekday', 'weekend']

    X = df.values[:, [2, 4, 4, 10, 11, 12, 13]]
    # X = df.values[:, [10, 11, 12, 13]]
    Y = df.values[:, 3]
    feature_map = {"day": 0, "month": 1, "weekday": 2, "weekend": 3, "quantity": 4}
    train_x, test_x, train_y, test_y = train_test_split(X, Y, test_size=test_split, random_state=42)
    data = {'train': (train_x, train_y),
            'test': (test_x, test_y),
            "item_map": {},
            "item_labels": {},
            "user_map": {},
            "user_labels": {},
            "feature_map": feature_map
            }
    return data


def load_sales_order_data(filename, user_id=None, test_split=0.2):
    # columns:  outlet_id, sku_id, quantity, order_status, trans_date, rate, gross_price, price, promotion,
    # bill_discount, reason
    #
    # new columns: outlet_id, sku_id, quantity, order_status, trans_date, rate, gross_price,
    # price, promotion, bill_discount, reason, 'date_time',  'day', 'month', 'weekday', 'weekend'

    df = pd.read_csv(filename, usecols=None, engine='python', skiprows=0, skipfooter=0,
                     na_values=['nan', 'null'])
    user_id = 149
    if user_id is not None:
        df = df[df["outlet_id"] == user_id]
    df["date_time"] = pd.to_datetime(df['trans_date'], format='%Y-%M-%d')
    df["day"] = df["date_time"].apply(lambda x: x.day)
    df["month"] = df["date_time"].apply(lambda x: x.month)
    df["weekday"] = df["date_time"].apply(lambda x: x.weekday())
    df["weekend"] = df["date_time"].apply(lambda x: 1 if x.weekday() in [4, 5] else 0)

    X = df.values[:, [5, 6, 7, 8, 9, 12, 13, 14, 15]]
    Y = df.values[:, 2]
    feature_map = {}
    train_x, test_x, train_y, test_y = train_test_split(X, Y, test_size=test_split, random_state=42)
    data = {'train': (train_x, train_y),
            'test': (test_x, test_y),
            "item_map": {},
            "item_labels": {},
            "user_map": {},
            "user_labels": {},
            "feature_map": feature_map
            }
    return data


def load_traffic_data(filename=None, user_id=None, test_split=0.2):
    import pandas as pd
    if filename is None:
        filename = traffic_data_file
    df = pd.read_csv(filename, usecols=None, engine='python', skiprows=0, skipfooter=0,
                     na_values=['nan', 'null'])
    if user_id is not None:
        df = df[df["store_code"] == user_id]
    df["date_time"] = df["hour_timestamp"].apply(lambda x: timestamp_to_datetime(x, tz))
    df["day"] = df["date_time"].apply(lambda x: x.day)
    df["month"] = df["date_time"].apply(lambda x: x.month)
    df["weekday"] = df["date_time"].apply(lambda x: x.weekday())
    df["weekend"] = df["date_time"].apply(lambda x: 1 if x.weekday() in [4, 5] else 0)

    df = df[df["traffic_count"] > 0]
    X = df.values[:, [5, 6, 7, 8]]
    Y = df.values[:, 3]
    feature_map = {"day": 0, "month": 1, "weekday": 2, "weekend": 3, "quantity": 4}

    train_x, test_x, train_y, test_y = train_test_split(X, Y, test_size=test_split, random_state=42)
    data = {'train': (train_x, train_y),
            'test': (test_x, test_y),
            "item_map": {},
            "item_labels": {},
            "user_map": {},
            "user_labels": {},
            "feature_map": feature_map
            }
    return data


def load_bike_share_data(test_split=0.2, name="day"):
    if name == "day":
        filename = bike_share_day_dataset_file
    else:
        filename = bike_share_hour_dataset_file
    df = pd.read_csv(filename, usecols=None, engine='python', skiprows=0, skipfooter=0,
                     na_values=['nan', 'null'])
    X = df.values[:, 2:-1]
    Y = df.values[:, -1]
    feature_map = {}
    train_x, test_x, train_y, test_y = train_test_split(X, Y, test_size=test_split, random_state=42)
    data = {'train': (train_x, train_y),
            'test': (test_x, test_y),
            "item_map": {},
            "item_labels": {},
            "user_map": {},
            "user_labels": {},
            "feature_map": feature_map
            }
    return data


def load_buzz_data(test_split=0.2, name="twitter"):
    if name == "twitter":
        filename = buzz_twitter_dataset_file
    else:
        filename = buzz_th_dataset_file
    df = pd.read_csv(filename, usecols=None, engine='python', skiprows=0, skipfooter=0,
                     na_values=['nan', 'null'])
    X = df.values[:, 0:-1]
    Y = df.values[:, -1]
    feature_map = {}
    train_x, test_x, train_y, test_y = train_test_split(X, Y, test_size=test_split, random_state=42)
    data = {'train': (train_x, train_y),
            'test': (test_x, test_y),
            "item_map": {},
            "item_labels": {},
            "user_map": {},
            "user_labels": {},
            "feature_map": feature_map
            }
    return data
