
import logging
import os
import pickle
import re
import time
from datetime import datetime, timedelta
import random
import numpy as np
import pytz
from pandas import Series
from sklearn.metrics import r2_score

logger = logging.getLogger(__name__)

def today_start_time():
    current_date = datetime.now()
    start_date = datetime(current_date.year, current_date.month, current_date.day, 0, 1, 1)
    return int(start_date.timestamp() * 1000)


def today_end_time():
    current_date = datetime.now()
    end_date = datetime(current_date.year, current_date.month, current_date.day, 23, 59, 59)
    return int(end_date.timestamp() * 1000)


def month_delta(date, delta):
    m, y = (date.month + delta) % 12, date.year + (date.month + delta - 1) // 12
    if not m:
        m = 12
    d = min(date.day, [31,
                       29 if y % 4 == 0 and not y % 400 == 0 else 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m - 1])
    return date.replace(day=d, month=m, year=y)


def previous_month(current_month_ts, tz='utc'):
    current_month_dt = datetime.fromtimestamp(current_month_ts / 1e3, tz=pytz.timezone(tz))
    previous_month_dt = month_delta(current_month_dt, -1)
    return np.int64(time.mktime(previous_month_dt.timetuple()) * 1000)


def timestamp_to_datetime(timestamp, tz='utc'):
    return datetime.fromtimestamp(timestamp / 1e3, pytz.timezone(tz))


def str_to_timestamp(string, tz="utc"):
    dt = datetime.strptime(string, '%Y-%m-%d').replace(tzinfo=pytz.timezone(tz))
    return np.int64(dt.timestamp() * 1000)


def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x))

    return e_x / e_x.sum(axis=None)


def hamilton_allocation(ratios, k):
    frac, results = np.modf(k * ratios)
    remainder = np.int32(k - np.sum(results))

    indices = np.argsort(frac)[::-1]

    if isinstance(results, Series):
        results[indices[0:remainder].index] += 1
    else:
        results[indices[0:remainder]] += 1
    return results


def r2_accuracy(targets, outputs):
    return r2_score(targets, outputs)


def save_to_pickle_file(filename, data):
    with open(filename, 'wb') as f:
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)


def load_from_pickle_file(filename):
    try:
        print("Reading file", filename)
        with open(filename, 'rb') as f:
            data = pickle.load(f)
            return data
    except Exception as e:
        logger.exception(e)
        return None


def mkdirs(path):
    try:
        os.makedirs(path, exist_ok=True)  # Python>3.2
    except TypeError:
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            pass


def get_unique_filename(prefix=None, extension=".jpg"):
    if prefix is not None:
        filename = str(prefix) + "_" + datetime.now().strftime("%Y_%m_%d_%H_%M_%S_%f")
        filename += extension
    else:
        filename = datetime.now().strftime("%Y_%m_%d_%H_%M_%S_%f")
        filename += extension
    return filename


def get_base_name(path):
    base = os.path.splitext(os.path.basename(path))[0]

    if not base:
        base = path
    base = re.sub('[:/]+', '_', base)
    base = re.sub('^[_+]|[_+]$', '', base)
    return base


def read_file(filename):
    """
    :param filename:
    :return: list of lines in the given file
    """
    return np.array([line.rstrip('\n') for line in open(filename)])


def map_kv(key, map_dict, inverse=False, default=None):
    if inverse:
        for k, v in map_dict.items():
            if key == v:
                return k
        return default
    else:
        return map_dict.get(key, default)


def random_date(start, end):
    """Generate a random datetime between `start` and `end`"""
    return start + timedelta(
        # Get a random amount of seconds between `start` and `end`
        seconds=random.randint(0, int((end - start).total_seconds())),
    )


def min_max_normalize(x, map_max=5, map_min=0, categorical=True):
    min_value = np.min(x)
    max_value = np.max(x)+1
    y = (x - min_value) * (map_max - map_min) / (max_value - min_value) + map_min
    if categorical:
        y = np.ceil(y)
    return y


if __name__ == '__main__':
    print(today_start_time(), today_end_time())
