
import collections
import logging
import os

import numpy as np
import scipy.sparse as sp
from lightfm import LightFM
from lightfm.evaluation import precision_at_k, auc_score, recall_at_k

from dataset import combine_features, create_context_features
from utils import load_from_pickle_file, save_to_pickle_file, map_kv, mkdirs

logger = logging.getLogger(__name__)


class Recommender(object):
    def __init__(self, model_dir=None):
        if model_dir is not None:
            model_file = os.path.join(model_dir, "recome_model.pkl")
            temp = load_from_pickle_file(model_file)
            if temp is not None:
                self.model = temp["model"]
                self.user_map = temp["user_map"]
                self.item_map = temp["item_map"]
                self.user_labels = temp["user_labels"]
                self.item_labels = temp["item_labels"]
                self.is_meta_features = temp["is_meta_features"]
                self.is_indicator_features = temp["is_indicator_features"]
                self.is_context = temp["is_context"]

                item_features_file = os.path.join(model_dir, "item_features.pkl")
                user_features_file = os.path.join(model_dir, "user_features.pkl")
                context_features_file = os.path.join(model_dir, "interactions.pkl")

                self.user_features = load_from_pickle_file(user_features_file)
                if self.user_features is not None:
                    self.user_features, _ = combine_features(self.user_features, self.is_indicator_features,
                                                             self.is_meta_features)

                self.item_features = load_from_pickle_file(item_features_file)
                if self.item_features is not None:
                    self.item_features, _ = combine_features(self.item_features, self.is_indicator_features,
                                                             self.is_meta_features)

                if self.is_context:
                    self.context_features = load_from_pickle_file(context_features_file)

    def train(self, dataset, model_dir):
        logger.info("Training...")
        train = dataset["train"]
        test = dataset["test"]
        is_context = False
        if len(np.shape(train)) == 3:
            is_context = True

            if type(train) != sp.lil.lil_matrix or type(train) != sp.coo.coo_matrix:
                train = train[:, :, 0]
                test = test[:, :, 0]

        if type(train) != sp.lil.lil_matrix or type(train) != sp.coo.coo_matrix:
            train = sp.lil_matrix(train).tocoo()
            test = sp.lil_matrix(test).tocoo()

        item_features = dataset['item_features']
        user_features = dataset['user_features']
        model = LightFM(no_components=32, learning_rate=0.001, loss='warp')
        model.fit(train, epochs=200, item_features=item_features, user_features=user_features, num_threads=8,
                  verbose=True)

        train_precision = precision_at_k(model, train, k=10, item_features=item_features,
                                         user_features=user_features).mean()
        test_precision = precision_at_k(model, test, k=10, item_features=item_features,
                                        user_features=user_features).mean()

        train_recall = recall_at_k(model, train, k=10, item_features=item_features, user_features=user_features).mean()
        test_recall = recall_at_k(model, test, k=10, item_features=item_features, user_features=user_features).mean()

        train_auc = auc_score(model, train, item_features=item_features, user_features=user_features).mean()
        test_auc = auc_score(model, test, item_features=item_features, user_features=user_features).mean()

        logger.info('Precision: train %.2f, test %.2f.' % (train_precision, test_precision))
        logger.info('Recall: train %.2f, test %.2f.' % (train_recall, test_recall))
        logger.info('AUC: train %.2f, test %.2f.' % (train_auc, test_auc))

        self.model = model
        self.user_map = dataset["user_map"]
        self.item_map = dataset["item_map"]
        self.user_labels = dataset["user_labels"]
        self.item_labels = dataset["item_labels"]
        self.is_meta_features = dataset["is_meta_features"]
        self.is_indicator_features = dataset["is_indicator_features"]
        self.user_features = user_features
        self.item_features = item_features
        self.is_context = is_context
        mkdirs(model_dir)
        model_file = os.path.join(model_dir, "recome_model.pkl")
        save_to_pickle_file(model_file,
                            {"model": model, "user_map": self.user_map, "item_map": self.item_map,
                             "user_labels": self.user_labels, "item_labels": self.item_labels,
                             "is_indicator_features": self.is_indicator_features,
                             "is_meta_features": self.is_meta_features, "is_context": is_context})

    def _predict_without_context(self, user_ids, item_ids=None, item_features=None,
                                 user_features=None, top_k=None, num_threads=1):
        if item_ids is None:
            item_ids = list(self.item_map.keys())

        user_ids_ = np.repeat(np.int32(user_ids), len(item_ids))

        item_ids_ = []
        item_ids_.extend(item_ids for _ in range(len(user_ids)))
        item_ids_ = np.array(item_ids_).flatten()

        predictions = self.model.predict(user_ids=user_ids_, item_ids=item_ids_, item_features=self.item_features,
                                         user_features=self.user_features, num_threads=num_threads)

        predictions = np.reshape(predictions, (len(user_ids), len(item_ids)))

        sorted_index = np.argsort(-predictions, axis=1)
        if top_k is not None:
            top_k = np.minimum(top_k, len(item_ids))

            sorted_index = sorted_index[:, :top_k]

        outputs = collections.OrderedDict()
        for i in range(len(sorted_index)):
            items = list(
                map(lambda j: [sorted_index[i, j], predictions[i, sorted_index[i, j]]], range(len(sorted_index[i]))))
            if len(items) == 0 or len(items[0]) == 0:
                items = None
            outputs[user_ids[i]] = items
        return outputs

    def predict_without_context(self, user_ids, item_ids=None, item_features=None,
                                user_features=None, top_k=None, num_threads=1):
        """
        Predict with actual outlet ids and sku ids
        """
        user_ids_ = [map_kv(k, self.user_map, inverse=False) for k in user_ids]
        assert None not in user_ids_, "Unknown user id(s)."

        item_ids_ = None
        if item_ids is not None:
            item_ids_ = [map_kv(k, self.item_map, inverse=False) for k in item_ids]
            item_ids_ = list(filter(lambda x: x is not None, item_ids_))

        outputs_ = self._predict_without_context(user_ids_, item_ids_, item_features, user_features, top_k, num_threads)
        outputs = collections.OrderedDict()
        for k_, v_ in outputs_.items():
            k = map_kv(k_, self.user_map, inverse=True)
            v = None
            if v_ is not None:
                v = list(map(lambda i: [map_kv(v_[i][0], self.item_map, inverse=True)] + v_[i][1:], range(len(v_))))
            outputs[k] = v
        return outputs

    def _contextual_items(self, uid, feature, top_k=None):
        scores = self.context_features[uid].dot(feature)
        item_norms = np.linalg.norm(self.context_features[uid], axis=1)
        item_norms[item_norms == 0] = 1e-10
        scores = np.divide(scores, item_norms)
        top_k = top_k or -1
        best = np.argsort(scores)[::-1][:top_k]
        results = list(map(list, zip(best, scores[best])))
        results = list(filter(lambda x: x[1] > 0, results))
        return results

    def _predict(self, user_ids, item_ids=None, item_features=None, user_features=None, top_k=None,
                 num_threads=1, contexts=None, context_only=False):
        if item_ids is None:
            item_ids = list(self.item_map.values())

        # Contextual processing
        context_results = []
        if self.context_features is not None and contexts is not None:
            context_features = create_context_features(contexts)
            for i in range(len(user_ids)):
                items = self._contextual_items(uid=user_ids[i], feature=context_features[i])
                context_results.append(items)

        model_results = []
        if not context_only or not contexts:
            user_ids_ = np.repeat(np.int32(user_ids), len(item_ids))

            item_ids_ = []
            item_ids_.extend(item_ids for _ in range(len(user_ids)))
            item_ids_ = np.array(item_ids_).flatten()

            print(self.user_features.shape[0])
            print(self.item_features.shape[0])

            predictions = self.model.predict(user_ids=user_ids_, item_ids=item_ids_, item_features=self.item_features,
                                             user_features=self.user_features, num_threads=num_threads)

            predictions = np.reshape(predictions, (len(user_ids), len(item_ids)))
            sorted_index = np.argsort(-predictions, axis=1)

            model_results = []
            for i in range(len(user_ids)):
                model_results.append(list(map(list, zip(sorted_index[i], predictions[i][sorted_index[i]]))))

        if model_results:
            max_score = np.max(model_results, axis=1)
            max_score = np.maximum(max_score, 0)
            temp = []
            for i in range(len(context_results)):
                temp.append(list(map(lambda x: (x[0], x[1] + max_score[i][1]), context_results[i])))
            context_results = temp

        results = []
        if context_results:
            for i in range(len(user_ids)):
                items = collections.OrderedDict()
                if context_results[i]:
                    for e in context_results[i]:
                        items[e[0]] = list(e)

                    if model_results:
                        for e in model_results[i]:
                            if e[0] not in items:
                                items[e[0]] = list(e)

                elif model_results:
                    for e in model_results[i]:
                        if e[0] not in items:
                            items[e[0]] = list(e)

                results.append(list(items.values()))
        else:
            results = model_results

        results = list(results)

        if top_k is not None:
            top_k = np.minimum(top_k, len(item_ids))

            for i in range(len(user_ids)):
                results[i] = results[i][:top_k]

        outputs = collections.OrderedDict()

        for i in range(len(user_ids)):
            items = results[i]
            if len(items) == 0 or len(items[0]) == 0:
                items = None
            outputs[user_ids[i]] = items
        return outputs

    def predict(self, user_ids, item_ids=None, item_features=None,
                user_features=None, top_k=None, num_threads=1, contexts=None, context_only=False):
        """
        Predict with actual outlet ids and sku ids
        """
        user_ids_ = [map_kv(k, self.user_map, inverse=False) for k in user_ids]
        assert None not in user_ids_, "Unknown user id(s)."

        item_ids_ = None
        if item_ids is not None:
            item_ids_ = [map_kv(k, self.item_map, inverse=False) for k in item_ids]
            item_ids_ = list(filter(lambda x: x is not None, item_ids_))

        print(item_ids)

        outputs_ = self._predict(user_ids_, item_ids_, item_features, user_features, top_k, num_threads,
                                 contexts=contexts, context_only=context_only)
        outputs = collections.OrderedDict()
        for k_, v_ in outputs_.items():
            k = map_kv(k_, self.user_map, inverse=True)
            v = None
            if v_ is not None:
                v = list(map(lambda i: [map_kv(v_[i][0], self.item_map, inverse=True)] + v_[i][1:], range(len(v_))))
            outputs[k] = v
        return outputs

    def _similar_items(self, item_id, item_features=None, top_k=None):
        outputs = {}
        item_id_ = map_kv(item_id, self.item_map, inverse=False)
        if item_id_ is None:
            outputs[item_id] = None
            return outputs

        (item_biased, item_representations) = self.model.get_item_representations(features=self.item_features)
        # Cosine similarity
        scores = item_representations.dot(item_representations[item_id])
        item_norms = np.linalg.norm(item_representations, axis=1)
        item_norms[item_norms == 0] = 1e-10
        scores /= item_norms
        best = np.argpartition(scores, -top_k)[-top_k:]
        results_ = sorted(zip(best, scores[best] / item_norms[item_id]),
                          key=lambda x: -x[1])
        results = list(
            map(lambda i: [map_kv(results_[i][0], self.item_map, inverse=True), results_[i][1]],
                range(len(results_))))
        results = list(filter(lambda x: x is not None, results))
        outputs[item_id] = results
        return outputs

    def similar_items(self, item_ids, item_features=None, top_k=None):
        outputs = collections.OrderedDict()

        for item_id in item_ids:
            output = self._similar_items(item_id=item_id, item_features=item_features, top_k=top_k)
            outputs.update(output)

        return outputs

    def _similar_users(self, user_id, user_features=None, top_k=None):
        outputs = {}
        user_id_ = map_kv(user_id, self.user_map, inverse=False)
        if user_id_ is None:
            outputs[user_id] = None
            return outputs

        (user_biased, user_representations) = self.model.get_user_representations(features=self.user_features)
        # Cosine similarity
        scores = user_representations.dot(user_representations[user_id])
        user_norms = np.linalg.norm(user_representations, axis=1)
        user_norms[user_norms == 0] = 1e-10
        scores /= user_norms
        best = np.argpartition(scores, -top_k)[-top_k:]
        results_ = sorted(zip(best, scores[best] / user_norms[user_id]),
                          key=lambda x: -x[1])
        results = list(
            map(lambda i: [map_kv(results_[i][0], self.user_map, inverse=True), results_[i][1]],
                range(len(results_))))
        results = list(filter(lambda x: x is not None, results))
        outputs[user_id] = results

        return outputs

    def similar_users(self, user_ids, user_features=None, top_k=None):
        outputs = collections.OrderedDict()

        for user_id in user_ids:
            output = self._similar_users(user_id=user_id, user_features=user_features, top_k=top_k)
            outputs.update(output)

        return outputs
